# Assignment 9

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a09`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```

Each file must have a
```shell
#!/usr/bin/env python3
```
line at the very top, and must be made executable with a command
```shell
chmod a+x p1.py
```
(same for `p2.py` ...)




### Problem 1 (*100%*)

Imagine we were using *heptadecimal* number system with base 17, where the digits are
```
1	=	1  (decimal)
...
9	=	9  (decimal)
a	=	10 (decimal)
b	=	11 (decimal)
c	=	12 (decimal)
d	=	13 (decimal)
e	=	14 (decimal)
f	=	15 (decimal)
g	=	16 (decimal)
```

That is, a number `7g` equals `7 * 17 + 16 = 135`.
Write a function `heptadecimal(n)` which takes an integer number `n` as an argument and
converts it into a string representing the heptadecimal number notation. So, if we run
`heptadecimal(135)` it would return just `'7g'`. No prefix of type `'0x'` is needed.
However, the conversion to heptadecimal must be done using **division** by 17,
that is, using the `//` (double-slash) operation (if you remember, in class we did
10 to 16 conversion using the remainder operation `%` — you should *not* use it now).
This way, the string will be built from left to right.
In the example above, the function would first find digit `'7'`, and the next iteration
would find digit `'g'` and append it
